# do-it-js

## Background
This project was originally created in CodePen and I have decided to move it over to my BitBucket account :)

The goal of this project was to create a vanilla JS library which can be added to any project. It leverages Javascript's in built Prototype object and is self contained so it can be easily dumped into another project. Browser cookies are also used in order to feel like a real to do list. No gulp, npm, babel etc. just some vanilla love.

## Usage
Simple instantiate an object and pass in:

1. The input ID
2. The button ID
3. The list ID
4. And whether or not you want the awesome easter egg to appear!

```javascript
// inputID, button trigger, list container, show easter egg?
DOIT('item-add__text', 'item-add__button', 'item-list__list', true);
```

## Demo
You can see it in action here: https://codepen.io/TomAlti93/pen/QJmarY
