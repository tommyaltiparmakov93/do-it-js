(function(global) {

	var DOIT = function(inputID, buttonID, listContainerID, egg) {
		return new DOIT.init(inputID, buttonID, listContainerID, egg);
	}

	var todosList = [];

	DOIT.prototype = {
		logTargets: function() {
			console.log(this.inputID, this.buttonID, this.listContainerID, this.egg);
		},

		// If input field is not empty
		validate: function() {
			if(document.getElementById(this.inputID).value !== '') return true;
		},

		// Add strings to array
		// Set input to empty
		// Update HTML based on array
		// Convert array to JSON and add to local storage
		addTodo: function() {
			if(this.validate() === true) {
				todosList.push(document.getElementById(this.inputID).value);
				document.getElementById(this.inputID).value = '';
				localStorage.setItem('todos', JSON.stringify(todosList));
				this.updateListHTML();
				this.isEgg();
			}
		},

		// Remove item in array based on index
		// Convert updated array to JSON and add to local storage
		// Update HTML based on array
		removeTodo: function(listItemID) {
			todosList.splice(listItemID, 1);
			localStorage.setItem('todos', JSON.stringify(todosList));
			this.updateListHTML();
		},

		// Read array and create HTML list
		updateListHTML: function() {
			var str = '<ul>';

			todosList.forEach(function(todo, index) {
				str += '<li class="list-item" data-id="' + index + '">' + todo + '</li>';
			});

			str += '</ul>';

			document.getElementById(this.listContainerID).innerHTML = str;
		},

		// Grab local storage data if any and set that to the array
		// Update list HTML
		getLocalTodos: function() {
			if (localStorage.getItem('todos') !== null) {
				todosList = JSON.parse(localStorage.getItem('todos'));
				this.updateListHTML();
			}
		},

		isEgg: function() {
			// Setup egg
			if(this.egg === true) {
				document.getElementById('egg').classList.remove('egg--out');
				document.getElementById('egg').classList.add('egg--active');
				document.getElementById('egg__img').src = 'https://media.giphy.com/media/qDPg6HNz2NfAk/giphy.gif';
				setTimeout(function() {
					document.getElementById('egg').classList.remove('egg--active');
					document.getElementById('egg').classList.add('egg--out');
					document.getElementById('egg__img').src = '';
				}, 1700);
			}
		}

	}

	DOIT.init = function(inputID, buttonID, listContainerID, egg) {
		// Init settings
		var _this = this;
		_this.inputID = inputID || '';
		_this.buttonID = buttonID || '';
		_this.listContainerID = listContainerID || '';
		_this.egg = egg;

		// Get local todos
		_this.getLocalTodos();

		// Create egg
		if(_this.egg === true) {
			var eggImg = document.createElement('img');
			eggImg.id = 'egg__img';
			var eggContainer = document.createElement('div');
			eggContainer.id = 'egg';
			eggContainer.classList.add('egg');
			eggContainer.appendChild(eggImg);
			document.body.appendChild(eggContainer);
		}

		// Events
		document.getElementById(_this.buttonID).addEventListener('click', function() {
			_this.addTodo();
		});

		document.getElementById(_this.inputID).addEventListener('keyup', function(event) {
			event.preventDefault();
			if(event.keyCode === 13) {
				document.getElementById(_this.buttonID).click();
			}
		});

		document.addEventListener('click', function (event) {
			if (!event.target.matches('.list-item')) return;
			event.preventDefault();
			_this.removeTodo(event.target.getAttribute('data-id'));
		});
	}

	global.DOIT = DOIT;

	DOIT.init.prototype = DOIT.prototype;

}(window));

// inputID, button trigger, list container, show easter egg?
DOIT('item-add__text', 'item-add__button', 'item-list__list', true);
